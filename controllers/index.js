'use strict';
var http = require('http');
var Room = require('../models/room');
var User = require('../models/user');

//所有来自不同域名的请求的重定向控制

module.exports = function(app) {
    var defaultView = 'index/index';
    var indexMAP = {
        'tongji.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '同济大学'
            }
        },
        'fudan.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '复旦大学'
            }
        },
        'ecnu.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '华东师范大学'
            }
        },
        'sjtu.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '上海交通大学'
            }
        },
        'sjtujiajiao.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '上海交通大学家教部'
            }
        },
        'yiban.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '易班'
            }
        },
        'sjtuhome.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '上海交通大学生活园区'
            }
        },
        'sjtujwc.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '上海交通大学教务处'
            }
        },
        'hgmx.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '沪港名校1V1'
            }
        },
        'siyuan.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '思源公益'
            }
        },
        'tecc.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: 'TECC(Technology & Education: Connecting Cultures)'
            }
        },
        'gongyi.haijiaoedu.com': {
            view: 'welcome',
            data: {
                school: '海角公益'
            }
        },
        'default': {
            view: 'welcome',
            data: {
                school: '海角课堂'
            }
        }
    };

    // 首页
    app.get('/', function(req, res) {
        var host = req.host && req.host.split(':')[0];
        var config = indexMAP['default'];
        var schoolConfig = indexMAP[host];
        // 房间总数
        var roomCondition = {};
        // 开放授权且可用的
        var openRoomCondition = {
            open: true,
            // 过滤已经结束的课程
            clock_state: {
                $ne: 'end'
            },
            // 12小时以内
            create_time: {
                $gte: new Date(Date.now() - 12 * 60 * 60 * 1000)
            }
        };

        if (schoolConfig) {
            roomCondition.school = host;
            openRoomCondition.school = host;
        }

        config = schoolConfig || config;
        config = Object.create(config);

        // 开放房间
        Room.find(openRoomCondition).count().exec(function(err, count) {
            config.data.open_rooms_count = count;
            // 房间总数
            Room.find(roomCondition).count().exec(function(err, count) {
                config.data.rooms_count = count;
                res.render(config.view, config.data);
            });
        });
    });

    // 查询IP所在地
    app.get('/getip', function(req, res) {
        var ip = req.param('ip');
        http.get('http://xytools.duapp.com/api/ip.php?ip=' + ip, function(response) {
            response.on('data', function(chunk) {
                res.jsonp(JSON.parse(chunk));
            });
        }).on('error', function(e) {
            res.jsonp({
                code: -1,
                data: e.message
            });
        });
    });

};