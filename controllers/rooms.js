'use strict';

var Room = require('../models/room');
var User = require('../models/user');

var cookieOption = {
    maxAge: 365 * 24 * 60 * 60 * 1000,
    //domain: ''
};

module.exports = function(app) {
    function createUser(req, res) {
        var uname = req.param('uname') || req.cookies.uname;
        var args = arguments;
        /*jshint validthis:true */
        var self = this;

        User.create({
            name: uname,
            ip: req.ip
        }, function(err, user) {
            if (err) {
                return res.showJSONError('很抱歉，用户创建失败！', [user, err]);
            }
            // 写cookie
            res.cookie('uid', user._id, cookieOption);
            res.cookie('uname', user.name, cookieOption);

            self.owner = user;
            createRoom.apply(self, args);
        });
    }
    // 创建房间
    function createRoom(req, res) {
        var name = req.param('name');
        var note = req.param('note');
        var open = req.param('open') || false;
        /*jshint validthis:true */
        var owner = this.owner;

        owner.login_time = Date.now();
        Room.create({
            create_time: owner.login_time, // 创建时间
            name: name, // 房间名称
            note: note, // 备注
            open: open,
            school: req.host, // 所属学校
            users: [owner], // 房间用户，第一位进入者为创始人
            online: [] // 当前在线用户
        }, function(err, room) {
            if (err) {
                return res[req.xhr ? 'showJSONError' : 'showError']('很抱歉，房间创建失败！', [room, err]);
            }
            if (req.xhr) {
                res.json(room);
            } else {
                res.redirect('/rooms/' + room._id);
            }
        });
    }

    // 创建房间
    app.post('/rooms', function(req, res) {
        var uname = req.param('uname') || req.cookies.uname;
        var name = req.param('name');
        var uid = req.cookies.uid;
        var args = arguments;
        /*jshint validthis:true */
        var self = this || {};

        if (!name || !uname) {
            return res.showJSONError('参数不完整！', [{
                name: name,
                uname: uname
            }]);
        }

        // 先检查用户是否已经存在
        if (!uid) {
            createUser.apply(self, args);
        } else {
            User.findById(uid, function(err, user) {
                if (err || !user) {
                    console.error(req.path, uid, '无效的用户id', err);
                    return createUser.apply(self, args);
                }

                // 标识房间创建用户
                self.owner = user;
                // 修改名字
                if (user.name !== uname) {
                    user.update({
                        name: uname,
                        ip: req.ip
                    }, function(err) {
                        if (err) {
                            return res.showJSONError('很抱歉，用户信息更新失败', [uid, err]);
                        }
                        res.cookie('uname', uname, cookieOption);
                        createRoom.apply(self, args);
                    });
                } else {
                    createRoom.apply(self, args);
                }
            });
        }
    });


    // 将用户加入房间用户列表中
    function insertRoomUser(req, res) {
        /*jshint validthis:true */
        var user = this.user;
        /*jshint validthis:true */
        var room = this.room;

        // 管理员进入
        if (room.users.length === 1 && ('' + room.users[0]._id === '' + user._id)) {
            console.log('add new user ' + user.name + ' to room [' + room.name + ']');
            res.render('main', room);
            return false;
        }

        // 记录登入时间
        user.login_time = Date.now();
        room.update({
            $push: {
                users: user
            }
        }, function(err) {
            if (err) {
                return res.showError('很抱歉，加入房间失败！', [room._id, user, err]);
            }
            console.log('add new user ' + user.name + ' to room [' + room.name + ']');
            res.render('main', room);
        });
    }


    var jsdom = require('jsdom');
    var fs = require('fs');
    var jquery = fs.readFileSync(__dirname + '/../public/static/lib/js/jquery-1.10.2.min.js', 'utf-8');
    var staticData = {
        teachers: 0,
        students: 0
    };
    // 抓取学生和老师数据，每半小时抓取一次
    function crawler() {
        jsdom.env({
            url: 'http://haijiaoedu.com/',
            src: [jquery],
            done: function(errors, window) {
                if (errors) {
                    console.log('crawler encounter errors:', errors);
                    return false;
                }
                var $ = window.$;
                staticData.teachers = /(\d+)/.test($('#hjltj').text()) && RegExp.$1;
                staticData.students = /(\d+)/.test($('#hjrtj').text()) && RegExp.$1;
            }
        });

        setTimeout(crawler, 30 * 60 * 1000);
    }
    crawler();

    // 进入房间
    app.get('/rooms/:id', function(req, res) {
        var roomId = req.param('id');
        var uid = req.cookies.uid;
        var uname = req.cookies.uname;
        var args = arguments;
        /*jshint validthis:true */
        var self = this || {};

        // 教室和学生数据
        res.locals(staticData);

        /**
         * 判断是否该房间的用户
         * @param  {Array}  users 房间用户列表
         * @return {Boolean}   是该房间用户则返回true
         */
        Room.findById(roomId, function(err, room) {
            // 当前用户是当前房间用户
            if (err) {
                return res.showError('很抱歉，你访问的房间不存在！', [roomId, err]);
            }
            if (!room) { // 房间不存在
                return res.showError('很抱歉，你访问的房间不存在！', [roomId, '房间不存在']);
            }

            // 房间超过4个人，新用户不允许进入
            if (room.online.length >= 4 && (!uid || !~room.online.indexOf(uid))) {
                return res.showError('很抱歉，该房间人数已满，请联系管理员！', [roomId, '房间人数已满(users:' + room.online.length + ')']);
            }

            self.room = room;

            if (uid) {
                User.findById(uid, function(err, user) {
                    if (err || !user) {
                        console.error(req.path, uid, '无效的用户id', err);
                        // 用户不存在，新建用户
                        User.create({
                            name: uname,
                            ip: req.ip
                        }, function(err, user) {
                            if (err) {
                                return res.showError('很抱歉，用户创建失败！', [user, err]);
                            }
                            // 写cookie
                            res.cookie('uid', user._id, cookieOption);
                            res.cookie('uname', user.name, cookieOption);

                            self.user = user;
                            insertRoomUser.apply(self, args);
                        });
                        return false;
                    }

                    // 标识房间创建用户
                    self.user = user;
                    insertRoomUser.apply(self, args);
                });
            } else {
                res.render('main', room);
            }
        });
    });
};