'use strict';

var moment = require('moment');
var Feedback = require('../models/feedback');

module.exports = function(app) {
    // 反馈信息列表
    app.get('/feedbacks', function(req, res) {
        var pagesize = 10,
            page = req.param('page') ? req.param('page') : 1;

        Feedback.find({}).count().exec(function(err, count) {
            var pages = Math.max(1, Math.ceil(count / pagesize));
            if (page < 1) {
                page = 1;
            } else if (page > pages) {
                page = pages;
            }
            Feedback.find({}).limit(pagesize)
                .skip(pagesize * (page - 1))
                .sort({_id: 'desc'})
                .exec(function(err, feedbacks) {
                    var data = [];
                    feedbacks = (feedbacks || []).map(function(item) {
                        item = item.toObject();
                        item.pub_time = moment(item.pub_time).format('YYYY-MM-DD HH:mm:ss');
                        return item;
                    });

                    res.render('feedbacks/index', {
                        feedbacks: feedbacks,
                        page: page,
                        pages: pages
                    });
                });
        });
    });

    // 单条反馈详情
    app.get('/feedbacks/:id', function(req, res){
        var feedbackId = req.param('id');

        Feedback.findById(feedbackId, function(err, feedback){
                        // 当前用户是当前房间用户
            if (err) {
                return res.showError('很抱歉，你查看的反馈建议不存在！', [feedbackId, err]);
            }
            if (!feedback) { // 房间不存在
                return res.showError('很抱歉，你查看的反馈建议不存在！', [feedbackId, '反馈建议不存在']);
            }
            feedback = feedback.toObject();
            feedback.pub_time = moment().format('YYYY-MM-DD HH:mm:ss');
            res.render('feedbacks/view', feedback);
        });
    });

    // 发布反馈建议
    // 此处允许匿名用户发布
    app.post('/feedbacks', function(req, res) {
        var uname = req.param('uname') || req.cookies.uname;
        var uid = req.cookies.uid;
        var title = req.param('title');
        var content = req.param('content');
        /*jshint validthis:true */
        var self = this;

        if (!title || !content) {
            return res.showJSONError('参数不完整！', [{
                title: title,
                content: content
            }]);
        }

        Feedback.create({
            uname: uname || '未知',
            uid: uid || 0,
            title: title,
            content: content,
            pub_time: Date.now()
        }, function(err, feedback) {
            if (err) {
                return res.showJSONError(err.message, [feedback, err]);
            }
            res.json({
                success: true,
                results: feedback
            });
        });
    });
};