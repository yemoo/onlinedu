'use strict';

var nodeMailer = require('nodemailer');
var from = 'admin@haijiaoedu.com';
var smtpTransport = nodeMailer.createTransport('SMTP', {
    host: 'smtp.ym.163.com',
    port: 25,
    //secureConnection:'true',
    auth: {
        user: from,
        pass: 'lejingwei'
    },
    //debug: true
});

module.exports = function(app) {
    app.post('/mails/send', function(req, res) {
        var data = {
            to: req.param('to'),
            fromUser: req.param('uname'),
            roomId: req.param('roomId')
        };
        var roomUrl = req.protocol + '://' + req.headers.host + '/rooms/' + data.roomId;

        if (!data.roomId || !data.to || !data.fromUser) {
            return res.showJSONError('信息输入不完整', [data]);
        }

        res.render('mail_template', {
            fromUser: data.fromUser,
            roomUrl: roomUrl
        }, function(err, html) {
            if (err) {
                return res.showJSONError(err.message, [data, err]);
            }

            smtpTransport.sendMail({
                to: data.to,
                from: from,
                subject: '用户邀请您使用海角课堂远程教学工具',
                html: html
            }, function(err, response) {
                smtpTransport.close();
                if (err) {
                    return res.showJSONError(err.message, [data, err]);
                }
                res.json({
                    success: true,
                    msg: '邮件发送成功'
                });
            });
        });
    });
};