'use strict';

var User = require('../models/user');
var Room = require('../models/room');

var cookieOption = {
    maxAge: 365 * 24 * 60 * 60 * 1000,
    //domain: ''
};

module.exports = function(app) {
    // 创建用户，新用户进入房间时执行
    app.post('/users', function(req, res) {
        var name = req.param('uname');
        var roomId = req.param('roomId');

        if (!name || !roomId) {
            return res.showJSONError('参数不完整', [{
                name: name,
                roomId: roomId
            }]);
        }

        Room.findById(roomId, function(err, room) {
            if (err) {
                return res.showJSONError('很抱歉，你访问的房间不存在！', [roomId, err]);
            }
            // 房间不存在
            if (!room) {
                return res.showJSONError('很抱歉，你访问的房间不存在！', [roomId, '房间不存在']);
            }
            // 房间人数达到上限,理论上执行不到这一步，除非程序post
            if (room.online.length >= 4) {
                return res.showJSONError('很抱歉，该房间人数已满，请联系管理员！', [roomId, '房间人数已满(users:' + room.online.length + ')']);
            }

            User.create({
                name: name,
                ip: req.ip
            }, function(err, user) {
                if (err) {
                    return res.showJSONError('很抱歉，用户创建失败！', [user, err]);
                }

                // 写cookie
                res.cookie('uid', user._id, cookieOption);
                res.cookie('uname', user.name, cookieOption);

                // 插入到room.users中
                user.login_time = Date.now();
                room.update({
                    $push: {
                        users: user
                    }
                }, function(err) {
                    if (err) {
                        return res.showJSONError('很抱歉，加入房间失败！', [roomId, user, err]);
                    }

                    res.json(user);
                });
            });
        });
    });

    // 获取用户信息
    app.get('/users/:id', function(req, res) {
        var uid = req.param('id');

        User.findById(uid, function(err, user) {
            if (err) {
                return res.showJSONError('该用户不存在', [uid, err]);
            }
            if (!user) {
                return res.showJSONError('该用户不存在', [uid, '用户不存在']);
            }
            res.json(user);
        });
    });
};