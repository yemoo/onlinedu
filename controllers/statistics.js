'use strict';

var moment = require('moment');
moment.lang('zh-cn');
var Room = require('../models/room');
var User = require('../models/user');


var schoolMap = {
    'tongji.haijiaoedu.com': '同济大学',
    'fudan.haijiaoedu.com': '复旦大学',
    'ecnu.haijiaoedu.com': '华东师范大学',
    'sjtu.haijiaoedu.com': '上海交通大学',
    'sjtujiajiao.haijiaoedu.com': '上海交通大学家教部',
    'yiban.haijiaoedu.com': '易班',
    'sjtuhome.haijiaoedu.com': '上海交通大学生活园区',
    'sjtujwc.haijiaoedu.com': '上海交通大学教务处',
    'hgmx.haijiaoedu.com': '沪港名校1V1',
    'siyuan.haijiaoedu.com': '思源公益',
    'tecc.haijiaoedu.com': 'TECC(Technology & Education: Connecting Cultures)',
    'gongyi.haijiaoedu.com': '海角公益'
};

/**
 * 格式化时间长度
 * @param  {date} duration momentjs-duration对象
 * @return {string}          格式化后的字符串
 */
function formatDuration(duration) {
    var results = [],
        tmp;

    if (tmp = duration.months()) {
        return '超过1个月';
    }
    if (tmp = duration.days()) {
        results.push(tmp + '天');
    }
    if (tmp = duration.hours()) {
        results.push(tmp + '小时');
    }
    if (tmp = duration.minutes()) {
        results.push(tmp + '分钟');
    }
    if (tmp = duration.seconds()) {
        results.push(tmp + '秒');
    }
    return results.join('');
}

/**
 * 格式化房间列表数据
 * @param  {array} rooms 原始房间列表数据
 * @return {array}       格式化后的房间列表
 */
function formatRoomData(rooms) {
    var now = Date.now();
    rooms = rooms || [];
    rooms = rooms.map(function(item) {
        item = item.toObject();

        // 创建时间
        item.create_time = moment(item.create_time).format('YYYY-MM-DD HH:mm:ss');

        // 开始上课时间
        if (item.begin_time) {
            item.begin_time = moment(item.begin_time).format('YYYY-MM-DD HH:mm:ss');
        }
        // 结束上课时间
        if (item.end_time) {
            item.end_time = moment(item.end_time).format('YYYY-MM-DD HH:mm:ss');
        }

        // 上课时长
        if (~['begin', 'resume'].indexOf(item.clock_state)) { // 如果进行中，计算当前时长
            item.duration = item.duration + (now - item.last_begin_time);
        }
        item.duration = item.duration && formatDuration(moment.duration(item.duration));

        // 学校名称
        item.schoolName = schoolMap[item.school] || (item.school === 'room.haijiaoedu.com' ? '海角课堂' : item.school);

        // 房间是否已满
        item.isfull = item.online.length >= 4;

        return item;
    });
    return rooms;
}

/**
 * 搜索房间列表
 * @param  {Object}   condition  搜索条件
 * @param  {Object}   pageConfig 分页参数
 * @param  {Function} callback   回调函数
 * @return {null}
 */
function searchRooms(condition, pageConfig, callback) {
    var pagesize = pageConfig.pagesize || 10;
    var page = pageConfig.page || 1;

    // 超过12小时一律设置为结束状态
    Room.update({
        // 超过12小时
        create_time: {
            $lt: new Date(Date.now() - 12 * 60 * 60 * 1000)
        },
        clock_state: {
            $ne: 'end'
        }
    }, {
        clock_state: 'end'
    }, {multi: true}, function(err, nums) {
        Room.find(condition).count().exec(function(err, count) {
            if (err) {
                return callback && callback(err);
            }
            var pages = Math.max(1, Math.ceil(count / pagesize));

            if (page < 1) {
                page = 1;
            } else if (page > pages) {
                page = pages;
            }

            Room.find(condition)
                .limit(pagesize)
                .skip(pagesize * (page - 1))
                .sort({
                    create_time: 'desc'
                })
                .exec(function(err, rooms) {
                    callback && callback(err, {
                        rooms: rooms,
                        pages: pages,
                        page: page
                    });
                });
        });
    });
}

module.exports = function(app) {
    // 开放授权房间列表
    app.all('/roomlist', function(req, res) {
        var search = req.body.search,
            from = schoolMap[req.host],
            condition = {
                // 开放授权的房间
                open: true,
                // 过滤已经结束的课程
                clock_state: {
                    $ne: 'end'
                }
            };

        // 指定学校的房间
        if (from) {
            condition.school = req.host;
        }

        // 用户搜索条件
        if (search && search.type) {
            condition[search.type] = new RegExp(search.keyword, 'i');
        }

        searchRooms(condition, {
            page: req.param('page'),
            pagesize: 10
        }, function(err, data) {
            if (err) {
                return res.showError(err.message, [condition, err]);
            }
            res.render('statistics/list', {
                rooms: formatRoomData(data.rooms),
                // 分页数据
                page: data.page,
                pages: data.pages,
                // 当前请求域名对应的学校
                from: from,
                // 搜索条件
                search: search
            });
        });
    });

    // 统计列表
    app.get('/statistics', function(req, res) {
        var from = schoolMap[req.host],
            condition = {};

        if (from) {
            condition.school = req.host;
        }

        searchRooms(condition, {
            page: req.param('page'),
            pagesize: 10
        }, function(err, data) {
            if (err) {
                return res.showError(err.message, [condition, err]);
            }
            res.render('statistics/admin', {
                rooms: formatRoomData(data.rooms),
                page: data.page,
                pages: data.pages,
                from: from
            });
        });
    });
};