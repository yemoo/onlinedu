'use strict';

var mongoose = require('mongoose');

var userModel = function() {
    var Schema = mongoose.Schema;
    //Define a super simple schema for our users.
    var userSchema = Schema({
        name: String, // 用户名
        login_time: Date,   // 登入时间
        ip: String  // ip地址
    });

    return mongoose.model('User', userSchema);

};

module.exports = new userModel();