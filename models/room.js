'use strict';

var mongoose = require('mongoose');
var ShortId = require('mongoose-shortid');

var roomModel = function() {
    var Schema = mongoose.Schema;
    //Define a super simple schema for our rooms.
    var roomSchema = Schema({
        _id: ShortId,
        create_time: Date,  // 创建时间
        name: String, // 房间名
        note: String, // 备注
        school: String, // 房间所属学校
        begin_time: Date, // 开始教学时间
        last_begin_time: Date, // 最后一次开始时间，继续上课的操作
        duration: Number, // 上课时间记时
        end_time: Date, // 教学结束时间
        clock_state: String,  // 最后一次记时操作
        owner: String, // 创始人
        open: Boolean,  // 是否公共授权房间
        users: [Schema.Types.Mixed], // 用户列表
        online: [String],  // 当前在线用户数, 只记录uid
        actions: [Schema.Types.Mixed] // 操作列表
    });

    return mongoose.model('Room', roomSchema);

};

module.exports = new roomModel();