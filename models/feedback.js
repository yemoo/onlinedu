'use strict';

var mongoose = require('mongoose');

var feedbackModel = function() {
    var Schema = mongoose.Schema;
    //Define a super simple schema for our users.
    var feedbackSchema = Schema({
        uid: String,    // uid
        uname: String, // 用户名
        title: String,  // 标题
        content: String,    //内容
        pub_time: Date  // 发布时间
    });

    return mongoose.model('Feedback', feedbackSchema);

};

module.exports = new feedbackModel();