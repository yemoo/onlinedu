// 房间管理
var RoomManager = {
    // 房间数据
    data: {
        socketServer: '',
        roomId: '',
        roomName: '',
        uname: '',
        uid: ''
    },
    clients: {},
    _inited: null,
    init: function(data) {
        var self = this;

        if (this._inited) return false;

        $.extend(this.data, data);

        this.initPageLayout();

        // 初始化用户信息
        this.initUserInfo();

        // 所有ajax请求都展示loadinginfo
        // var overlay = $("#js-loadingbar");
        // $(document).ajaxStart(function() {
        //     overlay.show();
        // }).ajaxComplete(function() {
        //     overlay.hide();
        // });

        // tooltip初始化
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });
        $(document).click(function(e) {
            if (!$(e.target).closest('[data-toggle=popover]').length) {
                $('[data-toggle=popover]').popover('hide');
            }
        });

        $('[data-toggle=popover]').popover({
            container: document.body,
            html: true,
            content: function() {
                return $(this).data('content') || $($(this).data('target')).html()
            }
        });
        // 聊天面板初始化
        // $('#chat_content').niceScroll({
        //     cursorwidth: 10
        // });

        this.checkUserinfo(function() {
            // 初始化用户邀请功能
            self.inviteInit();
            // 初始化socket
            self.initSocket();
        });

    },
    initPageLayout: function() {
        var sideExtra = 411 + 34 + 10;
        var updateHeight = function(el, height) {
            var oStyle;

            el = $(el);

            if ($.isArray(el)) {
                $.each(el, function(i, el) {
                    updateHeight(el, height);
                });
                return true;
            }

            oStyle = el.data('_oStyle');
            if (!oStyle) {
                el.data('_oStyle', oStyle = el.attr('style') || '');
            }

            if (height) {
                el.height(height);
            } else {
                el.attr('style', oStyle);
            }
        };
        var doc = $(document),
            sidebar = $('.sidebar'),
            main = $('.main'),
            chatContent = $('.chat-content'),
            defChatHeight = chatContent.height(),
            resizeTimer,
            height;

        var resizeLayout = function() {
            updateHeight([sidebar, main, chatContent]);
            height = Math.max($(window).height(), doc.height());

            updateHeight([sidebar, main], height);

            resizeChat();
        };

        var resizeChat = function() {
            updateHeight(chatContent);
            var chatHeight = Math.max(defChatHeight, $(window).height() + $(window).scrollTop() - sideExtra);
            // 聊天框始终位于左下侧
            updateHeight(chatContent, chatHeight);
        };

        this.initPageLayout = resizeLayout

        $(window).resize(resizeLayout);
        $(window).scroll(resizeChat);
        resizeLayout();
    },
    // 初始化用户信息
    initUserInfo: function() {
        // 用cookie初始化uid/uname
        if (!this.data.uid) {
            this.data.uid = $.cookie('uid', cookieConvertor);
        }
        if (!this.data.uname) {
            this.data.uname = $.cookie('uname', cookieConvertor);
        }
    },
    // 新用户提示创建昵称并加入房间
    checkUserinfo: function(callback) {
        if (!this.data.uid || !this.data.uname) {
            this.newUser(callback);
            return;
        } else {
            callback && callback();
        }
    },
    // 新用户加入
    newUser: function(callback) {
        var self = this;
        var userCreateModal = $('#modal-create-user');
        var userCreateForm = $('#form-create-user');

        // 创建新用户
        function createUser() {
            if (userCreateForm.valid()) {
                var formData = $.extend({}, self.data, queryStringToJSON(userCreateForm.serialize()));
                $.post('/users', formData, function(data) {
                    if (data.success === false) {
                        alert(data.error || '用户创建失败，请重试');
                        return false;
                    }
                    // 创建用户后初始化用户信息
                    self.initUserInfo();
                    userCreateModal.modal('hide');
                    callback && callback();
                });
            }
        };

        $('#btn-create-user').click(createUser);
        $('[name=uname]', userCreateForm).on('keypress', function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                createUser();
            }
        });

        // 显示用户昵称输入dialog
        userCreateModal.modal('show');
    },
    // 邮件邀请用户
    inviteInit: function() {
        var self = this;
        var inviteModal = $('#modal-invite-user');
        var inviteForm = $('#form-invite-user');

        function inviteUser() {
            if (inviteForm.valid()) {
                var formData = $.extend({}, self.data, queryStringToJSON(inviteForm.serialize()));
                if (formData.to) {
                    $.post('/mails/send', formData, function(data) {
                        if (!data.success) {
                            alert('很抱歉，邮件发送发送失败，您可以选择重试～' || data.msg);
                        } else {
                            alert(data.msg || '邀请成功，等候他(她)的加入把～');
                        }
                        inviteModal.modal('hide');
                    });
                } else {
                    inviteModal.modal('hide');
                }
            }
        }

        $('#btn-invite-user').click(inviteUser);
        $('[name=to]', inviteForm).keypress(function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                inviteUser();
            }
        });
    },
    // 初始化socket通信
    initSocket: function() {
        var self = this;
        var data = this.data;
        var socketServer = 'http://' + data.socketServer;
        var socket = this.socket = io.connect(socketServer);

        socket.on('connect', function() {
            console.log(data.uname + ' enter room [' + data.roomName + '] Successfully!');
			 
            // 绑定聊天室操作
            ChatAction.init(socket, data);
            // 绑定视频操作
            MediaAction.init(socket, data);
            // 绑定上课记时操作
            ClockAction.init(socket, data);
            // 画板
            BoardAction.init(socket, data);

            // 触发join事件
            socket.emit(Event.JOIN, data);
        });

        // 处理出错
        socket.on(Event.ERROR, function(data) {
            console.log(data);
        });
        // 房间用户信息
        socket.on(Event.CLIENTS, function(data){
            $.extend(self.clients, data);
        });
        socket.on(Event.JOIN, function(data){
            self.clients[data.id] = data;
			/*for(peerid in self.clients){
		       console.log("最新的人员列表为："+self.clients[peerid].uname); 
			}*/
        });
        socket.on(Event.LEAVE, function(data) {
            delete self.clients[data.id];
            console.log(data.uname + ' exit room [' + data.roomName + ']!');
        });
    }
};