function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

// String.format = function() {
//     if (arguments.length == 0)
//         return null;

//     var str = arguments[0];
//     for (var i = 1; i < arguments.length; i++) {
//         var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
//         str = str.replace(re, arguments[i]);
//     }
//     return str;
// }

function cookieConvertor(value) {
    if (value.indexOf('j:') == 0) {
        value = JSON.parse(value.substr(2));
    }
    return value;
}

function queryStringToJSON(qs) {
    var pairs = qs.split('&');
    var result = {};

    for (var idx in pairs) {
        var pair = pairs[idx].split('=');
        if ( !! pair[0]) {
            result[pair[0]] = decodeURIComponent(pair[1] || '');
        }
    }
    return result;
}

function parseURL(url) {
    var a =  document.createElement('a');
    a.href = url;
    return {
        source: url,
        protocol: a.protocol.replace(':',''),
        host: a.hostname,
        port: a.port,
        query: a.search,
        params: (function(){
            var ret = {},
                seg = a.search.replace(/^\?/,'').split('&'),
                len = seg.length, i = 0, s;
            for (;i<len;i++) {
                if (!seg[i]) { continue; }
                s = seg[i].split('=');
                ret[s[0]] = s[1];
            }
            return ret;
        })(),
        file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
        hash: a.hash.replace('#',''),
        path: a.pathname.replace(/^([^\/])/,'/$1'),
        relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
        segments: a.pathname.replace(/^\//,'').split('/')
    };
}