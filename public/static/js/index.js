$(function() {

    var roomCreateForm = $('#form-create-room');
    var roomCreateDialog = $('#modal-create-room');
    var elUname = roomCreateForm.find('[name=uname]');
    var uname = $.cookie('uname');

    // 如果已经有用户名，写入表单
    if (uname) {
        elUname.val(uname);
    }

    $('input', roomCreateForm).keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            createRoom();
        }
    });
    $('#btn-create-room').click(createRoom);

    function createRoom() {
        if (roomCreateForm.valid()) {
            // $.post('/rooms', roomCreateForm.serialize(), function(data) {
            //     var roomId = data._id;
            //     if (!roomId) {
            //         alert('很抱歉，房间创建失败，请重试～');
            //         return false;
            //     }
            //     roomCreateDialog.modal('hide');
            //     window.open('/rooms/' + roomId);
            // }, 'json');
            roomCreateDialog.modal('hide');
            roomCreateForm.submit();
            //roomCreateForm.find('[name=name],[name=note]').val('');
            setTimeout(function() {
                location.reload();
            }, 500);
        }
    }

    $('[data-toggle=tooltip]').tooltip({
        container: document.body,
        placement: 'bottom'
    });

    $(document).click(function(e) {
        if (!$(e.target).closest('[data-toggle=popover]').length) {
            $('[data-toggle=popover]').popover('hide');
        }
    });

    $('[data-toggle=popover]').popover({
        container: document.body,
        placement: 'top',
        html: true,
        content: function() {
            return $(this).data('content') || $($(this).data('target')).html()
        }
    });
});