function createAnchor(shapeLayer, isEditState, isEditShape) {
    var anchorColor = '#009DEC';
    var pointSize = 8;
    var positions = ['nw', 'n', 'ne', 'w', 'e', 'sw', 's', 'se'];
    var doc = $(document);
    var anchorLayer = new Kinetic.Layer({
        visible: false
    });
    shapeLayer.getStage().add(anchorLayer);

    // 解析位置
    function parsePosition(position) {
        if (position.length == 2) {
            return {
                v: position[0],
                h: position[1]
            };
        } else if (position == 'e' || position == 'w') {
            return {
                v: 'c',
                h: position
            };
        } else {
            return {
                v: position,
                h: 'c'
            };
        }
    }

    // 根据选择区域设置8角的位置
    function positionPoints() {
        var boxPos = box.position(),
            boxSize = box.size(),
            half = pointSize / 2;

        positions.forEach(function(position) {
            var pos = parsePosition(position),
                point = box[position];
            if (pos.v == 'n') {
                point.setY(boxPos.y - half);
            } else if (pos.v == 's') {
                point.setY(boxPos.y + boxSize.height - half);
            } else if (pos.v == 'c') {
                point.setY(boxPos.y + boxSize.height / 2 - half);
            }

            if (pos.h == 'w') {
                point.setX(boxPos.x - half);
            } else if (pos.h == 'e') {
                point.setX(boxPos.x + boxSize.width - half);
            } else if (pos.h == 'c') {
                point.setX(boxPos.x + boxSize.width / 2 - half);
            }
        });
    }

    // 同步拖动条
    function syncAnchorWithShape() {
        var shape = anchorLayer.bindShape;
        if (shape) {
            var shapePosition = shape.position();
            if (shape.className == "Ellipse") {
                shapePosition.x -= shape.radiusX();
                shapePosition.y -= shape.radiusY();
            }
            box.size(shape.size()).position(shapePosition);
            positionPoints();
            anchorLayer.show().draw();
        }
    }
    // 同步形状
    function syncShapeWithAnchor() {
        var shape = anchorLayer.bindShape;
        if (shape) {
            var position = box.position();
            if (shape.className == "Ellipse") {
                position.x += shape.radiusX();
                position.y += shape.radiusY();
            }
            shape.size(box.size()).position(position);

            doc.trigger('anchor_change', shape);
            positionPoints();
            shapeLayer.draw();
        }
    }

    function hideContainer() {
        if (curShape) {
            anchorLayer.bindShape = curShape;
            syncAnchorWithShape();
        } else {
            anchorLayer.hide();
            shapeLayer.draw();
        }
    }
    // 拖拽容器
    anchorLayer.on('mouseenter', function() {
        inShape = true;
    });
    // 移出后隐藏
    anchorLayer.on('mouseleave', function() {
        inShape = false;
        hideContainer();
    });
    anchorLayer.on('mousedown', function(evt) {
        curShape = anchorLayer.bindShape;
        doc.trigger('anchor_set', curShape);
    });

    // 外框
    var box = new Kinetic.Rect({
        stroke: anchorColor,
        strokeWidth: 1,
        draggable: true
    });
    box.on('mouseenter', function() {
        document.body.style.cursor = "move";
    });
    box.on('mouseleave', function() {
        document.body.style.cursor = "default";
    });
    box.on('dragmove', function() {
        syncShapeWithAnchor();
    });
    box.on('dragend', function() {
        doc.trigger('anchor_complete', anchorLayer.bindShape);
        shapeLayer.draw();
    });
    anchorLayer.add(box);
    // 8个拖拽点
    positions.forEach(function(position) {
        var curPosition;
        var point = new Kinetic.Rect({
            fill: anchorColor,
            stroke: anchorColor,
            strokeWidth: 1,
            width: pointSize,
            height: pointSize,
            draggable: true
        });
        point.on('mouseenter', function() {
            document.body.style.cursor = position + '-resize';
        });
        point.on('mouseleave', function() {
            document.body.style.cursor = 'default';
        });
        point.on('dragstart', function() {
            curPosition = this.position();
        });
        point.on('dragmove', function() {
            var pos = parsePosition(position);
            var pointPosition = point.position();

            if (pos.v == 'n') {
                box.setY(pointPosition.y + pointSize / 2);
                box.setHeight(curPosition.y - pointPosition.y + box.getHeight());
            } else if (pos.v == 's') {
                box.setHeight(pointPosition.y - curPosition.y + box.getHeight());
            }

            if (pos.h == 'w') {
                box.setX(pointPosition.x + pointSize / 2);
                box.setWidth(curPosition.x - pointPosition.x + box.getWidth());
            } else if (pos.h == 'e') {
                box.setWidth(pointPosition.x - curPosition.x + box.getWidth());
            }

            curPosition = pointPosition;
            anchorLayer.draw();

            syncShapeWithAnchor();
        });
        point.on('dragend', function() {
            doc.trigger('anchor_complete', anchorLayer.bindShape);
            shapeLayer.draw();
        });
        anchorLayer.add(box[position] = point);
    });


    var inShape, curShape;
    document.body.onclick = function(evt) {
        if (!inShape && !isEditShape(evt)) {
            curShape = null;
            doc.trigger('anchor_set', curShape);
            hideContainer();
        }
    };

    function showShapeAnchor(shape) {
        anchorLayer.bindShape = shape;
        syncAnchorWithShape();
    }

    function bindShape(shape) {
        shape.on('mouseenter', function() {
            if (isEditState()) {
                showShapeAnchor(shape);
            }
        });
    }

    return {
        bindShape: bindShape,
        setCurShape: function(shape) {
            if (shape) {
                inShape = true;
                showShapeAnchor(shape);
                anchorLayer.fire('click');
            } else {
                inShape = false;
                curShape = anchorLayer.bindShape = null;
                doc.trigger('anchor_set', curShape);
                hideContainer();
            }

        },
        getCurShape: function() {
            return curShape;
        },
        syncAnchorWithShape: syncAnchorWithShape
    };
}