
var Event = {
    CONNECT: 'connect',
    CONNECTING: 'connecting',
    DISCONNECT: 'disconnect',
    CONNECT_FAILED: 'connect_failed',
    RECONNECT: 'reconnect',
    RECONNECTING: 'reconnecting',
    ERROR: 'error',

    JOIN: 'join',
    LEAVE: 'leave',
    ACTION: 'action',
    ACTIONS: 'actions',
    WEBRTC: 'webrtc'
};

var STATE = {
    UNINIT: 0,
    CONNECTING: 1,
    CONNECTED: 2
};
// Socket连接管理
var Connect = {
    state: STATE.UNINIT, // socket状态
    socket: null,
    _inited: false,
    initialize: function(server, callback) {
        var self = this,
            socket;

        if (this._inited) return this;

        socket = this.socket = io.connect(server);
        self.state = STATE.CONNECTING;
        socket.on(Event.CONNECT, function() {
            self.state = STATE.CONNECTED;
            // 执行回调
            callback && callback.apply(this, arguments);
        });
        this._inited = true;
    },
    _check: function(method, callback) {
        if (this.state !== STATE.CONNECTED) {
            if (this.state === STATE.CONNECTING) {
                this.socket.on(Event.CONNECT, callback.bind(this));
                return false;
            }
            console.log('[Error]: socket未初始化前不能调用' + method + '方法');
            return false;
        }
        callback && callback.call(this);
    },
    // 发送消息
    send: function(event, data) {
        this._check('send', function() {
            this.socket.emit(event, data);
        });
    },
    // 监听消息
    listen: function(event, callback) {
        this._check('listen', function() {
            this.socket.on(event, function(data) {
                callback(data);
            });
        })
    }
};