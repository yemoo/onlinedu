
var Toolkit = {
    Pen: 0,
    Erase: 1,
    Text: 2,
    Hand: 3
};

function Board() {
    var self = this;

    this.container = 'drawContainer' + Room.curPage;
    this.initialized = false;
    this.stage = null;
    // is for line, shapes, eraser
    this.layer = null;
    // is for text or pictures that couldn't be changed
    this.layer1 = null;
    this.eraser = null;
    this.mouseDown = false;
    this.lastPos = null;
    this.textArea = null;
    this.text = null;
    this.line = null;

    this.tool = Toolkit.Pen;
    this._color = '#000';
    this._weight = 1;
    this.baseFontSize = 14;

    this.toolChanged = function() {
        switch (self.tool) {
            case Toolkit.Pen:
            case Toolkit.Erase:
                self.layer.moveUp();
                break;
            case Toolkit.Text:
            case Toolkit.Hand:
                self.layer1.moveUp();
                break;
        }
    };

    this.initialize = function(width, height) {
        if (self.stage) {
            self.stage.destroy();
        }

        var stage = self.stage = new Kinetic.Stage({
            container: self.container,
            width: width,
            height: height
        });

        var $container = $('#' + self.container);
        $(window).resize(function() {
            var newHeight = $container.height();
            if (newHeight && self.stage.getHeight() < newHeight) self.stage.setHeight(newHeight);
        });

        var layer = self.layer = new Kinetic.Layer();
        var layer1 = self.layer1 = new Kinetic.Layer();

        stage.add(layer, layer1);

        self.undos = [];
        $('#tool_undo').click(function() {
            var node = layer.getChildren().pop();
            if (node) {
                self.undos.push(node.remove());
                self.layer.draw();
            }
        });
        $('#tool_repeat').click(function() {
            var node = self.undos.pop();
            if (node) {
                self.layer.add(node);
                self.layer.draw();
            }
        });

        self.bindEvent();
        self.initilized = true;
    };

    this.mouseUp = function() {
        if (!self.mouseDown) return;
        self.mouseDown = false;

        if (self.tool === Toolkit.Pen && self.line) {
            self.bindRemoveShape(self.line);
            console.log(self.group.toJSON());
            Action.send(ActionType.LINE, {
                data: self.group.toJSON()
            });
        } else if (self.tool === Toolkit.Erase && self.eraser) {
            self.eraser = null;
            Action.send(ActionType.ENDERASE, {});
        } else if (self.tool === Toolkit.Text && self.textArea) {
            var left = self.textArea.getX();
            var top = self.textArea.getY();
            var width = self.textArea.getWidth();
            var height = self.textArea.getHeight();

            $text = $('<textarea class="board-text"></textarea>');
            $text.css({
                left: left,
                top: top,
                width: width + 'px',
                height: height + 'px',
                color: self.color,
                'font-size': (self.weight * self.baseFontSize) + 'px'
            });
            $('#' + self.container).append($text);
            self.text = $text;

            self.textArea.remove();
            self.textArea = null;
            self.layer1.draw();
        }
    };

    this.bindRemoveShape = function(shape) {
        /*shape.on('mousemove', function(){
           if(self.mouseDown && self.tool === Toolkit.Erase){
               console.log('mouse removing now!!!');
               shape.remove();
               self.layer.draw();
           } 
        });*/
    };

    this.bindEvent = function() {
        if (!self.stage) {
            return false;
        }

        self.stage.on('contentMousedown', function() {
            self.mouseDown = true;
            $(String.format('#{0} .board-text', self.container)).focusout();
            if (self.tool === Toolkit.Pen) {
                var mousePos = self.stage.getPointerPosition();

                self.lastPos = mousePos;

                self.line = new Kinetic.Line({
                    id: guid(),
                    points: [mousePos.x, mousePos.y, mousePos.x, mousePos.y],
                    stroke: self.color,
                    strokeWidth: self.weight,
                    lineJoin: 'round'
                });


                self.group = new Kinetic.Group({});
                self.group.add(self.line);

                self.layer.add(self.group);
                self.layer.draw();

                // Action.send(ActionType.LINE, {
                //     data: self.line.toJSON()
                // });

            } else if (self.tool === Toolkit.Erase) {
                var mousePos = self.stage.getPointerPosition();
                self.eraser = new Kinetic.Eraser({
                    points: [mousePos.x, mousePos.y, mousePos.x, mousePos.y],
                    strokeWidth: self.weight * self.baseFontSize,
                    lineJoin: 'round'
                });
                self.layer.add(self.eraser);
                self.layer.draw();
                Action.send(ActionType.ERASE, {
                    data: self.eraser.toJSON()
                });
            } else if (self.tool === Toolkit.Text && !self.text) {
                self.mouseDown = true;
                var mousePos = self.stage.getPointerPosition();
                self.lastPos = mousePos;

                self.textArea = new Kinetic.Rect({
                    x: mousePos.x,
                    y: mousePos.y,
                    width: 0,
                    height: 0,
                    strokeWidth: 1
                });

                self.layer1.add(self.textArea);
            } else if (self.text) {
                var content = self.text.val().trim();
                var top = parseInt(self.text.css('top'));
                var left = parseInt(self.text.css('left'));
                var width = parseInt(self.text.css('width'));
                var height = parseInt(self.text.css('height'));

                self.text.remove();
                self.text = null;

                if (content.length) {
                    var text = new Kinetic.SelectableText({
                        x: left + 1, // board
                        y: top + 1,
                        width: width,
                        height: height,
                        text: content,
                        fill: self.color,
                        fontSize: self.weight * self.baseFontSize
                    });
                    self.bindRemoveShape(text);
                    self.layer1.add(text);
                    self.layer1.draw();

                    Action.send(ActionType.TEXT, {
                        data: text.toJSON()
                    });
                }
            }
        });

        self.stage.on('contentMousemove', function() {
            if (!self.mouseDown) {
                return;
            } else if (self.tool === Toolkit.Pen && self.line) {
                var mousePos = self.stage.getPointerPosition();
                self.line.points(self.line.points().concat([mousePos.x, mousePos.y]));
                //self.group.add(self.)
                self.layer.draw();
                // Action.send(ActionType.APPEND, {
                //     pos: mousePos,
                //     tool: self.tool
                // });
            } else if (self.tool === Toolkit.Erase && self.eraser) {
                var mousePos = self.stage.getPointerPosition();
                self.eraser.points(self.eraser.points().concat([mousePos.x, mousePos.y]));
                self.layer.draw();
                Action.send(ActionType.APPEND, {
                    pos: mousePos,
                    tool: self.tool
                });
            } else if (self.tool === Toolkit.Text && self.textArea) {
                var mousePos = self.stage.getPointerPosition();
                var w = mousePos.x - self.lastPos.x;
                var h = mousePos.y - self.lastPos.y;
                if (w < 0) {
                    self.textArea.setX(mousePos.x);
                    self.textArea.setWidth(-w);
                } else {
                    self.textArea.setWidth(w);
                }

                if (h < 0) {
                    self.textArea.setY(mousePos.y);
                    self.textArea.setHeight(-h);
                } else {
                    self.textArea.setHeight(h);
                }

                self.layer1.draw();
            }

        });

        self.stage.on('contentMouseup', function() {
            self.mouseUp();
        });

        $(window).mouseup(function() {
            self.mouseUp();
        });

        return true;
    };

    this.addLine = function(json) {
        if (self.eraser) {
            self.eraser = null;
        }
        var owner = json.uid == Room.uid ? self : Room.contexts[json.uid];
        owner.line = Kinetic.Node.create(json.data);
        self.layer.add(owner.line);
        self.layer.draw();
    };
    this.addErase = function(json) {
        var owner = json.uid == Room.uid ? self : Room.contexts[json.uid];
        owner.eraser = Kinetic.Node.create(json.data);
        self.layer.add(owner.eraser);
        self.layer.draw();
    };
    this.endErase = function(json) {
        var owner = !json || json.uid == Room.uid ? self : Room.contexts[json.uid];
        owner.eraser = null;
    }
    this.appendLine = function(json) {
        var data = json;
        var owner = !json || json.uid == Room.uid ? self : Room.contexts[json.uid];
        switch (data.tool) {
            case Toolkit.Erase:
                if (owner.eraser) {
                    owner.eraser.points(owner.eraser.points().concat([data.pos.x, data.pos.y]));
                    self.layer.draw();
                }
                break;
            case Toolkit.Pen:
                owner.line.points(owner.line.points().concat([data.pos.x, data.pos.y]));
                self.layer.draw();
                break;
        }
    };

    this.addText = function(json) {
        var text = Kinetic.Node.create(json.data);
        self.layer1.add(text);
        self.layer1.draw();
    };
    this.updateText = function(json) {
        if (!json.id) return; // wrong data format
        var nodes = self.layer1.find('#' + json.id);
        if (nodes != null) nodes[0].setText(json.newText);
        self.layer1.draw();
    };
    this.moveText = function(json) {
        if (!json.id) return; // wrong data format
        var nodes = self.layer1.find('#' + json.id);
        if (nodes != null) nodes[0].position(json.pos);
        self.layer1.draw();
    };
    this.deleteText = function(json) {
        if (!json.id) return; // wrong data format
        var nodes = self.layer1.find('#' + json.id);
        if (nodes != null) nodes[0].remove();
        self.layer1.draw();
    };

    Object.defineProperty(this, 'weight', {
        set: function(w) {
            self._weight = w;
            if (self.text) {
                self.text.css('font-size', (w * self.baseFontSize) + 'px');
            }
        },

        get: function() {
            return self._weight;
        }
    });

    Object.defineProperty(this, 'color', {
        set: function(c) {
            self._color = c;
            if (self.text) {
                self.text.css('color', c);
            }
        },

        get: function() {
            return self._color;
        }
    });
}