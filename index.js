'use strict';


var kraken = require('kraken-js'),
    shortId = require('shortid'),
    mime = require('mime'),
    path = require('path'),
    db = require('./lib/database'),
    upload = require('jquery-file-upload-middleware'),
    locals = {},
    app = {},
    _nconf;


app.configure = function configure(nconf, next) {
    _nconf = nconf;

    //Configure the database
    db.config(nconf.get('databaseConfig'));

    // Async method run on startup.
    next(null);
};


app.requestStart = function requestStart(server) {
    // Run before most express middleware has been registered.
};


app.requestBeforeRoute = function requestBeforeRoute(app) {
    // 文件上传
    // uploader
    var uploadConfig = _nconf.get('uploadConfig');
    upload.configure({
        uploadDir: uploadConfig.path, // local directory
        uploadUrl: uploadConfig.download_url // upload file url
    });
    // rename
    upload.on('begin', function(fileInfo) {
        var ext = path.extname(fileInfo.name) || ('.' + mime.extension(fileInfo.type));
        fileInfo.name = shortId.generate() + ext;
    });

    app.use(uploadConfig.upload_url, upload.fileHandler());

    // global template vars
    app.locals({
        host: _nconf.get('host'),
        port: _nconf.get('port')
    });

    app.use(function(req, res, next) {
        res.locals.url = req.protocol + '://' + req.headers.host + '' + req.originalUrl;
        res.showJSONError = function(errMsg, log) {
            log.unshift(req.path);
            console.error.apply(console, log);
            res.send({
                success: false,
                error: errMsg
            });
        };
        res.showError = function(errMsg, log) {
            log.unshift(req.path);
            console.error.apply(console, log);
            res.render('error', {
                success: false,
                error: errMsg
            });
        };

        next(null);
    });
    // Run before any routes have been added.
};


app.requestAfterRoute = function requestAfterRoute(server) {
    // Run after all routes have been added.
};


if (require.main === module) {
    kraken.create(app).listen(function(err, server) {
        if (err) {
            return console.error(err.stack);
        }

        var fullHostName = _nconf.get('ssl') ? 'https://' : 'http://';
        fullHostName += _nconf.get('host') + ':' + _nconf.get('port');
        console.log('[FullHostName]: ' + fullHostName);

        // socket server        
        require('./lib/socket')(server);
    });
}


module.exports = app;