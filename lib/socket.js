'use strict';

var moment = require('moment');
var mongoose = require('mongoose');

var Room = require('../models/room');
var join = require('./sockets/join');
var classClock = require('./sockets/classclock');

// 自定义事件列表
var Event = {
    ERROR: 'error', // 发生错误

    JOIN: 'join', // 用户加入
    LEAVE: 'leave', // 用户离开
    ACTION: 'action', // 一次操作
    ACTIONS: 'actions', // 一组操作
    CLIENTS: 'clients', // 用户列表

    WEBRTC: 'webrtc', // webrtc connection
    CHAT: 'chat', // 聊天室
    CLOCK: 'clock' // 记时
};

function action(roomId, draw, callback) {

    Room.findById(roomId, function(err, room) {
        if (err) {
            console.log('Error: add action', draw, err);
            callback && callback(err);
            return false;
        } else if (!room) { // 房间不存在
            console.log('Error: add action', '房间不存在！', roomId);
            callback && callback(new Error('房间不存在'));
            return false;
        }

        // 每个action创建唯一id
        draw.id = new mongoose.Types.ObjectId();
        room.update({
            $push: {
                actions: draw
            }
        }, function(err) {
            if (err) {
                console.log('Error: add action', 'insert action', err);
                callback && callback(err);
                return false;
            }
            callback && callback(null, room);
        });
    });
}


// socket发送错误信息到客户端
function sendError(socket, type, err) {
    err = typeof err === 'object' ? err : new Error(err);
    socket.emit(Event.ERROR, {
        type: type,
        err: err
    });
    socket.emit(Event.ERROR + ':' + type, err);
}

/**
 * socket事件调度
 *
 *     Client: CONNECT -> Server: JOIN -> Room Clients: JOIN // 用户加入
 *                                     -> Client: ACTIONS
 *     Server: DISCONNECT -> Room Clients: LEAVE // 用户离开
 *
 * @param  {Object} server expressServer
 * @return {undefined}
 */
module.exports = function listen(server) {
    var io = require('socket.io').listen(server, {
        log: false
    });

    // 重启服务强制踢掉之前所有在线用户（僵尸用户）
    console.log('start kick all online users...');
    Room.update({}, {
        online: []
    }, function(err, nums) {
        if (err) {
            return console.log('Error: kick all online users', err);
        }
        console.log(nums + ' rooms has been force kill all users');
    });

    // 将所有之前在进行中的课程暂停掉
    console.log('start pause all running class...');
    Room.find({
        clock_state: {
            $in: ['begin', 'resume']
        }
    }, function(err, rooms) {
        if (err) {
            console.log('Error: pause all running class', err);
        }

        rooms.forEach(function(room) {
            classClock.updateRoomClockState(room, 'pause', function(err, data) {
                if (err) {
                    return console.log('Error: pause room', room.name, err);
                }
                console.log('room ' + room.name + ' has been paused!');
            });
        });
    });

    // 客户端连接成功
    io.sockets.on('connection', function(socket) {
        var self = this;
        // console.log('user connect');

        // 用户加入
        socket.on(Event.JOIN, function(data) {
            join.call(this, socket, data, function(err, room) {
                if (err) {
                    return sendError(socket, Event.JOIN, err);
                }

                data.id = socket.id;

                // toClient: 房间所有操作数据
                socket.emit(Event.ACTIONS, room.actions);

                // 房间用户列表
                var clients = {};
                io.sockets.clients(room.id).forEach(function(item){
                    clients[item.id] = item._data;
                });
                socket.emit(Event.CLIENTS, clients);

                // toClient: 房间记时状态
                socket.emit(Event.CLOCK, {
                    type: 'init',
                    clock_state: room.clock_state,
                    duration: room.duration || 0,
                    offset: Date.now() - room.last_begin_time
                });

                // 通知该房间所有clients
                socket.broadcast.to(room.id).emit(Event.JOIN, data);
            });
        });

        // 用户离开
        socket.on('disconnect', function() {
            var data = this._data;

            if (!data) {
                return console.log('Error: user leave room!');
            }

            Room.findByIdAndUpdate(data.roomId, {
                $pull: {
                    online: data.uid
                }
            }, function(err, room) {
                if (err) {
                    console.log('Error: leave room', data.roomName, err);
                    return false;
                } else if (!room) { // 房间不存在
                    console.log('Error: leave room', '房间不存在！', data.roomId);
                    return false;
                }

                console.log('user ' + data.uname + ' leave room [' + data.roomName + ']');
                // 离开时间
                data.leavetime = moment().format('YYYY-MM-DD HH:mm:ss');
                // 通知其他用户
                socket.broadcast.to(data.roomId).emit(Event.LEAVE, data);

                // 全部退出房间，自动结束课程
                if (room.online.length === 0 && room.clock_state) {
                    console.log('All users has exit the Room ' + data.roomName + ', auto end the class!');
                    classClock.updateRoomClockState(room, 'end', function(err, room) {
                        if (err) {
                            return console.log('Error: auto end class', data.roomName, err);
                        }
                    });
                }
            });
        });

        // 用户聊天
        socket.on(Event.CHAT, function(chatData) {
            var data = this._data;

            if (!data) {
                sendError(socket, Event.CHAT, '无效的socket连接');
                return console.log('Error: user Chat!');
            }

            // 发言者用户名
            chatData.uname = data.uname;
            // 发言时间
            chatData.time = moment().format('YYYY-MM-DD HH:mm:ss');
            // 通知其他用户
            socket.broadcast.to(data.roomId).emit(Event.CHAT, chatData);
        });

        // 用户视频共享
        socket.on(Event.WEBRTC, function(mediaData) {
            var data = this._data;

            if (!data) {
                sendError(socket, Event.WEBRTC, '无效的socket连接');
                return console.log('Error: user WebRTC!', data);
            }

            mediaData.from = data.id;

            // 点对点中关联对象应该为一个唯一id，不可用uid或者uname，
            // 因为一个用户有可能同时在两个房间里，单个uid/uname可能对应多个socket
            // 此处直接用socketId，确保唯一
            if ('to' in mediaData) {
                var peerSocket,
                    to = mediaData.to;

                delete mediaData.to;
                peerSocket = self.sockets[to];

                if (peerSocket) {
                    // 通知单点用户
                    peerSocket.emit(Event.WEBRTC, mediaData);
                }
            } else {
                // 通知房间所有用户
                socket.broadcast.to(data.roomId).emit(Event.WEBRTC, mediaData);
            }
        });

        // 用户上课记时
        socket.on(Event.CLOCK, function(type) {
            classClock.call(self, socket, type, function(err, room) {
                if (err) {
                    return sendError(socket, Event.CLOCK, err);
                }
                // 通知其他用户
                self['in'](room._id).emit(Event.CLOCK, {
                    type: type,
                    duration: room.duration
                });
            });
        });

        socket.on(Event.ACTION, function(data) {
            if (!socket._data) {
                console.log('Error: add action', '无效的连接数据', data, socket._data);
                sendError(socket, Event.ACTION, new Error('无效的连接数据'));
                return false;
            }

            var roomId = socket._data.roomId;
            // 处理过程不保存 直接转发房间用户
            if (~data.cate.indexOf('progress_')) {
                socket.broadcast.to(roomId).emit(Event.ACTION, data);
            } else {
                action.call(self, roomId, data, function(err, room) {
                    if (err) {
                        return sendError(socket, Event.ACTION, err);
                    }
                    socket.broadcast.to(roomId).emit(Event.ACTION, data);
                });
            }
        });
    });
};