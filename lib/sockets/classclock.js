'use strict';

var Room = require('../../models/room');

/**
 * 上课记时操作类型
 * @type {Object}
 */
var ClockAction = {
    BEGIN: 'begin',
    PAUSE: 'pause',
    RESUME: 'resume',
    END: 'end'
};
var ClockActionVals = Object.keys(ClockAction).map(function(key) {
    return ClockAction[key];
});
/**
 * 检测该操作是否有效
 * @param  {string}  state 操作类型
 * @param  {room}  room 房间对象
 * @return {Boolean}      有效返回true
 */
/*jshint maxcomplexity:10 */
function isValidRoomState(state, room, callback) {
    var curState = room.clock_state;
    if (!~ClockActionVals.indexOf(state)) { // 无效的房间操作
        console.log('Error: invalid clock action', state);
        callback && callback(new Error('无效的课程记时操作'));
    } else if (!curState) { // 未开始的房间
        if (state !== ClockAction.BEGIN) {
            // 未开始的课程不能执行开始之外的操作
            console.log('Error: can\'t operate the not begin class', curState);
            callback && callback(new Error('不能对一个未开始的课程执行开始之外的操作'));
        } else {
            return true;
        }
    } else { // 已经开始的房间
        if (state === ClockAction.BEGIN) {
            // 已经开始的课程，不能点开始上课
            console.log('Error: can\'t start the begined class', curState);
            callback && callback(new Error('不能开始一个已经开始的课程'));
        } else if (curState !== ClockAction.PAUSE && state === ClockAction.RESUME) {
            // 只有暂停状态下可以点击继续上课
            console.log('Error: can\'t resume the running class', curState, state);
            callback && callback(new Error('不能继续正在进行的课程'));
        } else if (curState === state) {
            // 本次操作与上次相同
            console.log('Error: don\'t run repeat operation!', state);
            callback && callback(new Error('不能执行与上次一样的操作'));
        } else if (curState === ClockAction.END) {
            // 已经结束的课程不允许任何操作
            console.log('Error: can\'t change the ended class:', state);
            callback && callback(new Error('不能修改已经结束的课程状态'));
        } else {
            return true;
        }
    }
    return false;
}
/**
 * 上课记时
 * @param  {socket} socket socket对象
 * @param  {string} state   操作类型
 * @return {undefined}
 */
function classClock(socket, state, callback) {
    var data = socket._data;

    if (!data) {
        console.log('Error: class clock', '无效的连接数据', state, data);
        callback && callback(new Error('无效的连接数据'));
        return false;
    }

    updateClockStateById(data.roomId, state, callback);
}
/**
 * 根据房间id更新房间记时类型
 * @param  {number}   roomId   房间id
 * @param  {string}   state     记时类型
 * @param  {Function} callback 回调函数
 * @return {null}
 */
function updateClockStateById(roomId, state, callback) {
    Room.findById(roomId, function(err, room) {
        if (err) {
            console.log('Error: class clock', state, err);
            callback && callback(err);
            return false;
        } else if (!room) { // 房间不存在
            console.log('Error: class clock', '房间不存在！', roomId);
            callback && callback(new Error('房间不存在'));
            return false;
        } else if (!isValidRoomState(state, room, callback)) { // 校验 点击的操作 是否有效
            console.log('Error: class clock', '无效的记时操作', state);
            callback && callback(new Error('无效的记时操作'));
            return false;
        }

        // 更新数据
        updateRoomClockState(room, state, callback);
    });
}

/**
 * 获取房间课程状态更新数据
 * @param  {Room} room 房间对象
 * @param  {string} state 操作类型
 * @return {object}      更新数据对象
 */
function getUpdateData(room, state) {
    // 更新数据
    var now = Date.now();
    var updateInfo = {
        clock_state: state // 操作类型
    };
    if (state === ClockAction.BEGIN) { // 开始上课
        updateInfo.begin_time = updateInfo.last_begin_time = now;
        updateInfo.duration = 0;
    } else if (state === ClockAction.PAUSE) { // 暂停上课
        updateInfo.duration = room.duration + (now - room.last_begin_time);
    } else if (state === ClockAction.RESUME) { // 继续上课
        updateInfo.last_begin_time = now;
    } else if (state === ClockAction.END) { // 结束上课
        if (room.clock_state !== ClockAction.PAUSE) {
            updateInfo.duration = room.duration + (now - room.last_begin_time);
        }
        updateInfo.end_time = now;
    }
    console.log('class room [' + room.name + '] is now ' + state);

    return updateInfo;
}

/**
 * 更新房间数据
 * @param  {room}   room    房间对象
 * @param  {string}   state  房间操作类型
 * @param  {Function} callback   回调函数
 * @return {null}
 */
function updateRoomClockState(room, state, callback) {
    var updateInfo = getUpdateData(room, state);
    room.update(updateInfo, function(err) {
        if (err) {
            console.log('Error: class clock', 'update data', err);
            callback && callback(err);
            return false;
        }
        callback && callback(null, room);
    });
}

classClock.ACTIONS = ClockAction;
classClock.getUpdateData = getUpdateData;

classClock.isValidRoomState = isValidRoomState;
classClock.updateRoomClockState = updateRoomClockState;
classClock.updateClockStateById = updateClockStateById;

module.exports = classClock;