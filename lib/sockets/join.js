'use strict';

var moment = require('moment');
var Room = require('../../models/room');

/**
 * 用户加入
 * @param  {Object} socket socket连接
 * @param  {Object} data   数据
 *         {
 *             socketServer: '',
 *             roomId: '',
 *             roomName: '',
 *             uname: '',
 *             uid: ''
 *         }
 * @return {undefined}
 */
function join(socket, data, callback) {
    // 确保有房间id和用户id
    if (!data) {
        console.log('Error: join room', '无效的连接数据', data);
        callback && callback(new Error('无效的连接数据'));
        return false;
    }

    // 保存用户信息到socket对象
    socket._data = data;

    console.log('user ' + data.uname + ' join room [' + data.roomName + ']');
    // 加入时间
    data.jointime = moment().format('YYYY-MM-DD HH:mm:ss');

    var roomId = data.roomId;
    // 确保为有效的房间
    Room.findById(roomId, function(err, room) {
        if (err) {
            console.log('Error: join room', data.roomName, err);
            callback && callback(err);
            return false;
        } else if (!room) { // 房间不存在
            console.log('Error: join room', '房间不存在！', roomId);
            callback && callback(new Error('房间不存在！'));
            return false;
        }

        // 新加入该房间的用户（重复进入不计）
        if (!~room.online.indexOf(data.uid)) {
            // 最多4个用户在线
            if (room.online.length >= 4) {
                console.log('Error: join room', '房间用户已满', roomId);
                callback && callback(new Error('房间用户已满'));
                return false;
            }

            room.update({
                $push: {
                    online: data.uid
                }
            }, function(err) {
                if (err) {
                    console.log('Error: join room', '房间加入失败', data.uid, roomId, err);
                    callback && callback(new Error('房间加入失败'));
                    return false;
                }
                // client连接放入该room
                socket.join(roomId);
                // 执行回调
                callback && callback(null, room);
            });
        } else {
            // client连接放入该room
            socket.join(roomId);
            // 执行回调
            callback && callback(null, room);
        }
    });
}
module.exports = join;